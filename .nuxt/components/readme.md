# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Cart>` | `<cart>` (components/Cart.vue)
- `<CartItem>` | `<cart-item>` (components/CartItem.vue)
- `<Detail>` | `<detail>` (components/Detail.vue)
- `<Error>` | `<error>` (components/error.vue)
- `<Header>` | `<header>` (components/Header.vue)
- `<Landing>` | `<landing>` (components/Landing.vue)
